students = {
  :cohort1 => 34,
  :cohort2 => 42,
  :cohort3 => 22
}

students[:cohort4] = 43

students.each do |cohort, number|
  puts "#{cohort}: #{number} students"
end

students.each do |cohort, number|
puts "#{cohort}"
end

students.each do |cohort, number|
  x = (number * 1.05).floor
  puts "#{cohort}: new number of students #{x}"
end

students.delete(:cohort2)
students.each do |cohort, number|
  puts "#{cohort}: #{number} students"
end

x =  students.inject(0){|result, (cohort, number)| result + number }
puts "The total number of students now is #{x}"
