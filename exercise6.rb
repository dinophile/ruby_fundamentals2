# Make an array
$groceries = ["apples", "bananas", "holy hand grenades", "salmon"]
$groceries.each do |item|
  puts "* #{item}"
end

puts "--------"

# Add a new item to the list and print it out again in this case we can add rice when prompted
def add_food
  puts "Add a new item to the grocery list:"
  food = gets.chomp
  $groceries.push(food)
  $groceries.each do |item|
    puts "* #{item}"
  end
end

add_food

puts "--------"

# Count the number of items on the list
puts $groceries.count

puts "--------"

# Find and item on your list, we can input Bananas
def find_item
  puts "Have you forgotten anything? Let's check now:"
  new_item = gets.chomp
  if $groceries.include?(new_item)
    puts "You don't need to pick up #{new_item} today!"
  else
    $groceries.push(new_item)
    puts "You need to pick up #{new_item}!"
  end
end

find_item

puts "--------"

# Alphabetize the grocery list
def sort_list
  $groceries.sort!
  $groceries.each do |item|
  puts "* #{item}"
end
end

sort_list

puts "--------"

# Delete the salmon and print out the new list
$groceries.delete("salmon")

$groceries.each do |item|
  puts "* #{item}"
end
